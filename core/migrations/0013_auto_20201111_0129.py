# Generated by Django 3.1.2 on 2020-11-11 04:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0012_auto_20201111_0126'),
    ]

    operations = [
        migrations.AlterField(
            model_name='chamados',
            name='prioridade',
            field=models.CharField(choices=[('BX', 'Baixa'), ('NR', 'Normal'), ('AL', 'Alta'), ('UR', 'Urgente')], default='Normal', max_length=7),
        ),
    ]
