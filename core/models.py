from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.db import models

# Create your models here.

PATENTE_CHOICES = [

    ('CMD', 'Comandante Geral'),
    ('CEL', 'Coronel'),
    ('TENCEL', 'Tenente-Coronel'),
    ('MAJ', 'Major'),
    ('CAP', 'Capitão'),
    ('TEN1', '1° Tenente'),
    ('TEN2', '2° Tenente'),
    ('ASP', 'Aspirante a Oficial'),
    ('SUB', 'SubTenente'),
    ('SGT1', '1° Sargento'),
    ('SGT2', '2° Sargento'),
    ('SGT3', '3° Sargento'),
    ('CB', 'Cabo'),
    ('SD', 'Soldado'),

]

TIPO_USER_CHOICES = [

    ('ADM', 'Administrador'),
    ('USER', 'User'),
    ('AUX', 'Suporte'),

]

STATUS_CHAMADO_CHOICES = [

    ('Aberto', 'Aberto'),
    ('Fechado', 'Fechado'),
    ('Concluido', 'Concluido'),
    ('Atrasado', 'Atrasado'),
    ('Atendimento', 'Atendimento'),

]

TIPO_CHAMADO_CHOICES = [

    ('Duvida', 'Duvida'),
    ('Sugestão', 'Sugestão'),
    ('Dificuldade de uso', 'Dificuldade de uso'),
    ('Defeito Observado', 'Defeito Observado'),
    ('Solicitação de Acesso', 'Solicitação de Acesso'),

]

PRIORIDADES_CHOICES = [
    ('BX', 'Baixa'),
    ('NR', 'Normal'),
    ('AL', 'Alta'),
    ('UR', 'Urgente')
]

AVALIACAO_CHOICES = [
    ('0', 0),
    ('1', 1),
    ('2', 2),
    ('3', 3),
    ('4', 4),
    ('5', 5)
]


class MeuUserManager(BaseUserManager):
    def create_user(self, matricula, email, nome, sobrenome, patente, password=None):
        if not matricula:
            raise ValueError("Matricula é obrigatória")
        if not nome and sobrenome:
            raise ValueError("Nome e Sobrenome são obrigatórios")
        if not patente:
            raise ValueError("Patente é obrigatória")
        if not email:
            raise ValueError("Email é obrigatório")

        user = self.model(
            matricula=matricula,
            email=self.normalize_email(email),
            nome=nome,
            sobrenome=sobrenome,
            patente=patente,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, matricula, email, nome, sobrenome, patente, password):
        user = self.create_user(
            matricula=matricula,
            email=self.normalize_email(email),
            nome=nome,
            password=password,
            sobrenome=sobrenome,
            patente=patente,
        )

        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    matricula = models.IntegerField(unique=True)
    email = models.EmailField(verbose_name="email", max_length=70, unique=True)
    nome = models.CharField(max_length=30, blank=False)
    sobrenome = models.CharField(max_length=30, blank=False)
    patente = models.CharField(max_length=6, choices=PATENTE_CHOICES, default='SD')
    tipo_user = models.CharField(max_length=4, choices=TIPO_USER_CHOICES, default='USER')
    date_joined = models.DateTimeField(verbose_name="Data do registro", auto_now_add=True)
    last_login = models.DateTimeField(verbose_name="Último login", auto_now=True)
    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)

    USERNAME_FIELD = 'matricula'
    REQUIRED_FIELDS = ['email', 'nome', 'sobrenome', 'patente']

    objects = MeuUserManager()

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return True


class Chamados(models.Model):
    cod_chamado = models.AutoField(primary_key=True)
    user_matricula = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name='user_matricula')
    status_chamado = models.CharField(max_length=11, choices=STATUS_CHAMADO_CHOICES, default='Aberto')
    data_hora = models.DateTimeField(auto_now=True)
    tipo_chamado = models.CharField(max_length=25, choices=TIPO_CHAMADO_CHOICES, default='Duvida')
    descricao = models.CharField(max_length=500, blank=False)
    prioridade = models.CharField(max_length=7, choices=PRIORIDADES_CHOICES, default='Normal')


class Avaliacao(models.Model):
    codigo_avaliacao = models.AutoField(primary_key=True)
    matricula_user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name='matricula_user')
    data_hora = models.DateTimeField(auto_now=True)
    comentario = models.CharField(max_length=300, blank=True)
    classificacao = models.CharField(max_length=1, choices=AVALIACAO_CHOICES, default='0')
    codigo_chamado = models.ForeignKey(Chamados, on_delete=models.SET_NULL, null=True, related_name='codigo_chamado')

