from django.http import request
from django.shortcuts import render, redirect, get_object_or_404
from .models import *
from core.forms import *
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required

# Create your views here.


def redirectlogin(request):
    return render(request, 'pre_system/Redirectlogin.html')



def registrar(request):
    form = UserForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            return redirect('login')
        else:
            form = UserForm()
    return render(request, 'pre_system/indexcadastrodeuser.html', {'form': form})


# Listar
@login_required
def listChamados(request):
    chamados = Chamados.objects.all()
    user = request.user.tipo_user
    return render(request, 'indexlistchamado.html', {'chamados': chamados, 'user': user})

@login_required
def listAvaliacao(request):
    avaliacoes = Avaliacao.objects.all()
    return render(request, 'indexlistavaliacao.html', {'avaliacoes': avaliacoes})

# Criar
@login_required
def createChamado(request):
    form = ChamadoForm(request.POST)
    if form.is_valid():
        form.save()
        return redirect('list')
    return render(request, 'indexcadastrarchamado.html', {'form': form})

@login_required
def createAvaliacao(request):
    avaliacao = AvaliacaoForm(request.POST)
    if avaliacao.is_valid():
        avaliacao.save()
        return redirect('list')
    return render(request, 'indexcreateavaliacao.html', {'avaliacao': avaliacao})

# Update
@login_required
def updateChamado(request, id):
    chamado = get_object_or_404(Chamados, pk=id)
    form = ChamadoForm(request.POST or None, instance=chamado)
    if form.is_valid():
        form.save()
        return redirect('list')
    return render(request, 'indexupdatechamado.html', {'form': form})

@login_required
def updateAvaliacao(request, id):
    avaliacao = get_object_or_404(Avaliacao, pk=id)
    formAvaliacao = AvaliacaoForm(request.POST or None, instance=avaliacao)
    if formAvaliacao.is_valid():
        formAvaliacao.save()
        return redirect('avaliacoes')
    return render(request, 'indexupdateavaliacao.html', {'formAvaliacao': formAvaliacao})

#Delete
@login_required
def deleteChamado(request, id):
    chamado = get_object_or_404(Chamados, pk=id)
    if request.method == 'POST':
        chamado.delete()
        return redirect('list')
    return render(request, 'deleteChamado.html', {'chamado': chamado})
