from django.forms import *
from django.contrib.auth.forms import UserCreationForm, forms
from .models import *
from . import models


class UserForm(UserCreationForm):
    matricula = forms.CharField(label='Matrícula', widget=forms.TextInput(
        attrs={'id': 'matricula', 'autofocus': 'autofocus', 'placeholder': 'Matrícula', 'class': 'form-control'}))
    nome = forms.CharField(label='Nome', widget=forms.TextInput(
        attrs={'id': 'nome', 'autofocus': 'autofocus', 'placeholder': 'Nome', 'class': 'form-control'}))
    sobrenome = forms.CharField(label='Sobrenome', widget=forms.TextInput(
        attrs={'id': 'sobrenome', 'autofocus': 'autofocus', 'placeholder': 'Sobrenome', 'class': 'form-control'}))
    email = forms.EmailField(label='Email', widget=forms.TextInput(
        attrs={'id': 'exampleInputEmail1', 'name': 'email', 'class': 'form-control', 'placeholder': 'Seu email'}),
                             max_length=150)
    patente = forms.CharField(label='Patente',
                              widget=forms.Select({'size': '1', 'class': 'form-control'}, choices=PATENTE_CHOICES))
    password1 = forms.CharField(label='Senha', widget=forms.TextInput(
        attrs={'type': 'password', 'id': 'exampleInputPassword1', 'minlength': '8', 'placeholder': 'Senha'}),
                                required=True)
    password2 = forms.CharField(label='Confirme sua Senha', widget=forms.TextInput(
        attrs={'type': 'password', 'id': 'exampleInputPassword1', 'placeholder': 'Confirme sua senha'}), required=True)

    class Meta:
        model = User
        fields = ['matricula', 'nome', 'sobrenome', 'email', 'patente', 'password1', 'password2']


class ChamadoForm(ModelForm):
    status_chamado = forms.CharField(label='Status do Chamado', widget=forms.Select({'size': '1', 'class': 'form-control'}, choices=STATUS_CHAMADO_CHOICES))
    tipo_chamado = forms.CharField(label='Tipo do Chamado', widget=forms.Select({'size': '1', 'class': 'form-control'}, choices=TIPO_CHAMADO_CHOICES))
    descricao = forms.CharField(label='Descrição', widget=forms.TextInput(attrs={'type': 'text', 'placeholder': 'Descrição'}), required=True)
    prioridade = forms.CharField(label='Prioridade', widget=forms.Select({'size': '1', 'class': 'form-control'}, choices=PRIORIDADES_CHOICES))

    class Meta:
        model = Chamados
        fields = ['cod_chamado', 'user_matricula', 'status_chamado', 'prioridade', 'tipo_chamado', 'descricao']


class AvaliacaoForm(ModelForm):
    comentario = forms.CharField(label='Comentário', widget=forms.TextInput(attrs={'type': 'text', 'placeholder': 'Deixe aqui seus comentários'}),
                                required=True)
    class Meta:
        model = Avaliacao
        fields = ['codigo_avaliacao', 'matricula_user', 'comentario', 'classificacao', 'codigo_chamado']