from django.urls import path, include
from .views import *

urlpatterns = [
    path('', redirectlogin),
    path('accounts/', include('django.contrib.auth.urls')),
    path('chamados/', listChamados, name='list'),
    path('cadastro/', registrar, name='registrar'),
    path('CriarChamado/', createChamado, name='create'),
    path('AtualizarChamado/<int:id>/', updateChamado, name='update'),
    path('DeletarChamado/<int:id>/', deleteChamado, name='delete'),
    path('Avaliacoes/', listAvaliacao, name='avaliacoes'),
    path('Avaliar/', createAvaliacao, name='avaliacao'),
    path('EditarAvaliacao/<int:id>/', updateAvaliacao, name='updateavaliacao')

]
